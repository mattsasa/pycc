import sys
from makeTokens import make_token_strings, make_tokens
from generateAST import generate_ast
from writeElf import writeCodeToElf
from generateMachineCode import Generator

if len(sys.argv) == 1:
    c_file_path = "test.c"
    outputFilename = "myElfTest.exe"
elif len(sys.argv) == 2:
    c_file_path = sys.argv[1]
    outputFilename = "elfs/" + c_file_path[6:][:-2] + ".exe"
elif len(sys.argv) == 3:
    c_file_path = sys.argv[1]
    outputFilename = "myElfTest.exe"
else:
    print("invalid usage")
    exit(1)

c_file = open(c_file_path,"r")

## Lexing
tokens = make_tokens(c_file.readlines())

## Parsing tokens and building Abstract Syntax Tree
ast = generate_ast(tokens)

## Generating Machine / Object Code / OpCodes
programBytes, dataBssBytes, memsize = Generator().generateMachineCode(ast)

## Write Bytes to Elf File
writeCodeToElf(programBytes, dataBssBytes, memsize, outputFilename)

