import sys
sys.path.insert(0, "link_libraries")
from constants import *
from util import toByteArray
import stdio

class FunctionsHandler:
    def __init__(self):
        self.functions = []
        self.functionCalls = []
        self.usedPrintf = False
        self.mostRecentFuncStartIndex = 0
        self.mostRecentFuncStartName = None

    def getFunctionByName(self, funcName):
        for function in self.functions:
            if function['name'] == funcName:
                return function
        return None

    def getFunctionArgs(self, functionName):
        found = False
        arguments = []
        for function in self.functions:
            if function['name'] == functionName:
                found = True
                arguments = function['args']
        if not found: throwError('Error: function ' + functionName + ' has not been declared')
        return arguments

    def addExternalFunctions(self, gen):
        if self.usedPrintf:
            for locations in stdio.printNumberLocalVariables.values():
                for index in locations:
                    stdio.printNumberBytes[index:index+4] = toByteArray(gen.code.bssIndex + mainFixedMemOffset)
                gen.code.bssIndex += 4

            self.functions.insert(0, { 'name': 'printNumber', 'code': stdio.printNumberBytes })