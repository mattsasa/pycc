mainFixedMemOffset = 0x0804B5E8

typeDataSizeTable = {
    'int': 4,
    'int8': 1,
    'char': 1,
    'bool': 1
}

comparisonToJumpOpCode = {
    '!=': 0x84,
    '==': 0x85,
    '<=': 0x8C,
    '>=': 0x8F,
    '<': 0x8E,
    '>': 0x8D
}