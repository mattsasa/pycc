from constants import *
from util import BasicObject, throwError

class Var:
    def __init__(self, name, offset, type):
        self.name = name
        self.memLocation = offset + mainFixedMemOffset
        self.dataSize = typeDataSizeTable[type]
        self.type = type

class VariablesHandler:
    def __init__(self):
        self.scopeLevel = 0
        self.scopes = [[]]

    def enterScope(self):
        self.scopeLevel += 1
        self.scopes.append([])

    def exitScope(self):
        self.scopeLevel -= 1
        self.scopes.pop()

    def getVar(self, variableName):
        for vars in reversed(self.scopes):
            varsWithName = list(filter(lambda var: var.name == variableName, vars))
            if len(varsWithName) > 0: break

        if len(varsWithName) == 0:
            throwError("Error var '" + variableName + "' has not been declared")
        return varsWithName[0]

    def createDataVar(self, varName, value, dataType, arraySize, code):
        newVar = Var(varName, code.bssIndex, dataType)
        code.bssIndex += newVar.dataSize * arraySize
        dataMemLocation = code.dataIndex + mainFixedMemOffset
        self.scopes[self.scopeLevel].append(newVar)
        if dataType == 'int':
            if not value.lstrip('-').isnumeric(): throwError('int types must be numbers')
            code.addDWordToData(int(value))
        elif dataType == 'int8':
            if not value.lstrip('-').isnumeric(): throwError('int types must be numbers')
            code.addByteToData(int(value))
        elif dataType == 'char':
            if len(value) != 3: throwError('Error - char should 1 character surronded with quotes')
            else: code.addByteToData(ord(value[1]))
        else: throwError("Unknown invalid dataSize")
        for i in range(0, arraySize):
            code.movMemoryToRegA(newVar.dataSize, dataMemLocation + (newVar.dataSize * i))
            code.movRegAtoMemory(newVar.dataSize, newVar.memLocation + (newVar.dataSize * i))
        return newVar

    def createBssVar(self, varName, dataType, code):
        newVar = Var(varName, code.bssIndex, dataType)
        self.scopes[self.scopeLevel].append(newVar)
        code.bssIndex += newVar.dataSize
        return newVar

    def createBssVarFromEaxValue(self, varName, dataType, code):
        ## Create a new var in bss Section
        newVar = self.createBssVar(varName, dataType, code)
        ## copy eax value into newvar memlocation  mov [var], eax
        code.movRegAtoMemory(newVar.dataSize, newVar.memLocation)

    def updateVarFromEaxValue(self, varName, code):
        newVar = self.getVar(varName)
        ## copy eax value into var memlocation  mov [var], eax
        code.movRegAtoMemory(newVar.dataSize, self.getVar(varName).memLocation)