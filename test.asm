section     .data

    msg     db  'Hello, world!',0xa                 ;our dear string
    len     equ $ - msg                             ;length of our dear string

section .bss           ;Uninitialized data
    meow    resd 1
    woof    resd 1
    small   resb 1

section     .text
global      _start                              ;must be declared for linker (ld)

_start:                                         ;tell linker entry point

    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    ;setting data to memory at meow
    mov     al, [eax]               ;8A00
    mov     al, [ebx]               ;8A03
    mov     [small], al             ;A2 01020304
    mov     dword [meow], 88888888  ;C705 04910408 08 ;copy immediate (8) to the location meow
    mov     ebx, 88888888
    mul     ebx            ;8005 04910408 30 ;add the immediate value (48) to the location meow
    mul     dword [meow]        ;12288   ;8005 04910408 FF?
    ;add     byte [meow], 4     ;8005 04910408 04 ;add the immediate value (4) to the location meow

    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel


    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel


    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel


    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    ;Prints Hello world with a new line
    mov     edx,len                 ;BA 0E000000 ;message length
    mov     ecx,msg                 ;B9 F4900408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    ;nothing section
    ;mov      eax, [meow]         ;A1 04910408         ;B8 04000000
    ;mov      [woof],eax          ;A3 2C910408
    ;add     eax, [meow]         ;03 05 04910408         ;83 C0 02
    ;mov     ebx, eax                ;89 C3

    ;Prints a single char at meow
    mov     edx,1                   ;BA 01000000 ;message length
    mov     ecx,woof                ;B9 04910408 ;address of message to write 0x08049104
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    ;Prints a single char at meow
    mov     edx,1                   ;BA 01000000 ;message length
    mov     ecx,woof                ;B9 04910408 ;address of message to write 0x08049104
    add     ecx,1
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

    mov     byte [meow], 0xa   ;C605 04910408 0A ;copy immediate (0x0A) to the location meow

    ;Prints a new line
    mov     edx,1                   ;BA 01000000 ;message length
    mov     ecx,meow                ;B9 04910408 ;address of message to write
    mov     ebx,1                   ;BB 01000000 ;file descriptor (stdout)
    mov     eax,4                   ;B8 04000000 ;system call number (sys_write)
    int     0x80                    ;CD 80       ;call kernel

_bottom:                                         ;tell linker entry point

    ;Exits with exit code
    mov     byte [meow], 7     ;C605 04910408 08 ;copy immediate (7) to the location meow
    mov     ebx, [meow]           ;8B1D 04910408 ;setting exit code to the value at meow
    mov     eax,1                                ;system call number (sys_exit)
    int     0x80                                 ;call kernel

