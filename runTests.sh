#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
WHITE='\033[0;37m'

totaltests=0
passedtests=0

for filename in elfs/*
do
  let totaltests++
  part1="${filename:5}"
  testname="${part1%????}"
  cfile="tests/$testname.c"
  lastline="$(tail -n 1 $cfile)"
  expected="${lastline:9}"
  ioline="$(tail -n 2 $cfile | head -n 1)"
  io="${ioline:8}"
  iotype="${ioline:2:5}"
  iolinelen=${#ioline}
  if [[ ${ioline:0:2} == "//" ]]
  then
    if [[ ${ioline:2:5} == "enter" ]]
    then
      ./expectbot.sh $filename $io &> /dev/null
    else
      $filename | grep "${io}" &> /dev/null
    fi
  else
     $filename &> /dev/null
  fi

  if [ $? -eq $expected ]
  then
    echo -e "${GREEN}Test: $testname succeeded"
    let passedtests++
  else
    echo -e "${RED}Test: $testname failed"
  fi
done

echo -e "${WHITE}$passedtests out of $totaltests tests passed!"
