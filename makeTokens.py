from util import *

split_chars = [' ', '(', ')', '{', '}', ';', '=', '+', '-', '*', '/', '%', ',', '<', '>', '[', ']', '!']
operators = ['+', '-', '*', '/', '%', '||', '&&']
comparisons = ['<', '>']
constructs = ['return', 'if', 'else', 'true', 'false', 'while', 'for']
dataTypes = ['int', 'float', 'char', 'int8', 'bool']

class Token:
    def print(self):
        return self.value + ' (' + self.type + ')'

def handleIncrementShorthand(tokens):
    i = 0
    while i < len(tokens):
        if tokens[i].value in ('+','-') and tokens[i].value == tokens[i+1].value:
            tokens[i].value = tokens[i].value + '='
            tokens[i].type = 'Assignment Operator'
            tokens[i+1].value = "1"
            tokens[i+1].type = 'Constant'
        i += 1
    return tokens

def mergeComparisons(tokens):
    i = 0
    while i < len(tokens):
        if tokens[i].value in ('>','<','=','!') and tokens[i+1].value == '=':
            tokens[i].value = tokens[i].value + '='
            tokens[i].type = 'Comparison'
            del tokens[i+1]
        i += 1
    return tokens

def mergeAssignmentOperators(tokens):
    i = 0
    while i < len(tokens):
        if tokens[i].value in ('+','-','%','*','/') and tokens[i+1].value == '=':
            tokens[i].value = tokens[i].value + '='
            tokens[i].type = 'Assignment Operator'
            del tokens[i+1]
        i += 1
    return tokens

def mergeNegativeNumbers(tokens):
    i = 1
    while i < len(tokens):
        if (tokens[i].value == '-' and tokens[i+1].type == 'Constant' and tokens[i+1].value.isnumeric() and
                                tokens[i-1].value in ('=','+','(','-','{','*','/','[','return', ',','<','>')):
            tokens[i].value = '-' + tokens[i+1].value
            tokens[i].type = 'Constant'
            del tokens[i+1]
        i += 1
    return tokens

def convertStringsToTokens(arr):
    tokens = []
    for str in arr:
        token = Token()
        if str.startswith('"') or str.startswith("'"): token.type = "Constant"
        elif str in split_chars:
            if str in operators: token.type = 'Operator'
            elif str in comparisons: token.type = 'Comparison'
            else: token.type = "Other Symbol"
        elif str.isnumeric(): token.type = "Constant"
        elif str in operators: token.type = 'Operator'
        elif str in constructs: token.type = "Construct"
        elif str in dataTypes: token.type = "Type"
        else: token.type = "Identifer"
        token.value = str
        tokens.append(token)
    return tokens

def skipCommentsAndNewLines(lines):
    arr = []
    for line in lines:
        if line != '\n':
            if line[-1] == '\n': subarr = [line[:-1]]
            else: subarr = [line]
            if subarr[0].lstrip(' ')[0] != '/': #ignore comments and empty lines
                arr.extend(subarr)
    return arr

def splitAtSymbols(arr):
    i = 0
    while i < len(arr):
        if len(arr[i]) > 1:
            for char in split_chars:
                if char in arr[i]:
                    splits = split_at_symbol(char, arr[i])
                    arr.pop(i)
                    i -= 1
                    j = i
                    for new in splits:
                        j += 1
                        arr.insert(j, new)
                    i += 1
        i += 1

def joinStringsWithinQuotes(arr):
    i = 0
    while i < len(arr):
        if arr[i][0] in ('"',"'") and arr[i][0] != arr[i][-1]:
            count = 0
            removeThese = []
            thechar = arr[i][0]
            j = i + 1
            growth = arr[i] + arr[j]
            removeThese.append(j)
            while arr[j][-1] != thechar:
                j += 1
                growth = growth + arr[j]
                removeThese.append(j)

            arr[i] = growth
            for index in removeThese:
                del arr[index-count]
                count += 1
        i += 1

def make_token_strings(lines):
    arr = skipCommentsAndNewLines(lines)
    splitAtSymbols(arr)
    joinStringsWithinQuotes(arr)
    return list(filter(lambda item: item != ' ', arr))

def make_tokens(lines):
    arr = make_token_strings(lines)
    tokens = convertStringsToTokens(arr)
    tokens = mergeComparisons(tokens)
    tokens = mergeAssignmentOperators(tokens)
    tokens = mergeNegativeNumbers(tokens)
    tokens = handleIncrementShorthand(tokens)
    return tokens