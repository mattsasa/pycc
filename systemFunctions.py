from util import *
from constants import *

class SystemFunctions:
    def __init__(self, generator, code):
        self.gen = generator
        self.code = code

    def getchar(self):
        self.code.setEAXtoSysRead()
        self.code.setEBXtoStdin()
        self.code.movImmediateToECX(self.code.bssIndex + mainFixedMemOffset) #dud address that will get overwritten
        self.code.movImmediateToEDX(1)
        self.code.callKernel()

        # Copy Result into EAX
        self.code.movMemoryToRegA(1, self.code.bssIndex + mainFixedMemOffset)

    def putchar(self, char):
        self.gen.putExpressionIntoEax(char)
        targetAddr = self.code.bssIndex + mainFixedMemOffset
        self.code.bssIndex += 1
        self.code.movRegAtoMemory(1, targetAddr)
        self.code.writeCodeForSysWrite(1, targetAddr)

    def printAStringVar(self, varName):
        var = self.gen.vars.getVar(varName)
        self.code.writeCodeForSysWrite(var.arraySize, var.memLocation)

    def printAStringImmediate(self, charList):
        addr = self.code.dataIndex + mainFixedMemOffset
        for char in charList:
            self.code.addByteToData(char)
        self.code.writeCodeForSysWrite(len(charList), addr)

    def printANumberVar(self, expression):
        self.gen.putExpressionIntoEax(expression)
        self.code.pushEAX()
        self.code.callRelative(0xFFFFFFFF) ## figure out offset later
        indexFromFuncStart = self.code.programIndex - self.gen.functions.mostRecentFuncStartIndex
        self.gen.functions.functionCalls.append({
            'name': 'printNumber',
            'indexFromFuncStart': indexFromFuncStart,
            'callingFunction': self.gen.functions.mostRecentFuncStartName
         })
        self.code.popECX()

    def printf(self, args):
        self.gen.functions.usedPrintf = True
        if len(args) == 1:
            if args[0].type == 'terminating' and args[0].token.type == 'Constant' and not args[0].token.value.isnumeric():
                self.printAStringImmediate(formatStrToCharArr(args[0].token.value)[0])
                self.printAStringImmediate([10])
            else:
                self.printANumberVar(args[0])
                self.printAStringImmediate([10])
        else:
            charListArr = formatStrToCharArr(args[0].token.value)
            argc = 1
            for i in range(0, len(charListArr)):
                if charListArr[i] == '%s':
                    self.printAStringVar(args[argc].token.value)
                    argc += 1
                elif charListArr[i] == '%d':
                    self.printANumberVar(args[argc])  #### To do create print a number var function
                    argc += 1
                else: self.printAStringImmediate(charListArr[i])