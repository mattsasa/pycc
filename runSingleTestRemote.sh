
if [ -z "$1" ]
then
	python3 pycc.py
else 
	python3 pycc.py $1 0
fi
scp myElfTest.exe root@snuffymatt.me:/home/nasm/myElfTest.exe
ssh root@snuffymatt.me /home/nasm/myElfTest.exe

echo Exit Code: `ssh root@snuffymatt.me echo $?`
