int main() {

    int c = getchar() - 48;

    if (c >= 5) {
      return 2;
    } else {
      return 1;
    }

    return 0;
}
//enter 4
//expect 1