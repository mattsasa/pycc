int main() {

    int x = 6;

    x += 2;
    x %= 3;
    x -= 5;
    x += 100;
    x /= 5;

    return x;
}
//expect 19