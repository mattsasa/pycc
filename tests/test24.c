int printNumber(int num) {

  if (num < 0) {
    putchar(45);
    printNumber(num * (0 - 1));
   } else {
     int digit5 = num / 10000;
     int remainder5 = num % 10000;
     int digit = remainder5 / 1000;
     int remainder = remainder5 % 1000;
     int digit2 = remainder / 100;
     int remainder2 = remainder % 100;
     int digit3 = remainder2 / 10;
     int remainder3 = remainder2 % 10;
     putchar(digit5 + 48);
     putchar(digit + 48);
     putchar(digit2 + 48);
     putchar(digit3 + 48);
     putchar(remainder3 + 48);
     putchar(10);
   }
}

int abs(int num) {
  if (num < 0) {return num * (0-1); }
  else { return num; }
}

int xValues[4];
int yValues[4];
int zValues[4];
int vxValues[4];
int vyValues[4];
int vzValues[4];

int updateGravity(int index1, int index2) {

  if (xValues[index1] > xValues[index2]) { vxValues[index1] = vxValues[index1] - 1; vxValues[index2] = vxValues[index2] + 1; }
  else { if (xValues[index1] < xValues[index2]) { vxValues[index1] = vxValues[index1] + 1; vxValues[index2] = vxValues[index2] - 1; } else { } }

  if (yValues[index1] > yValues[index2]) { vyValues[index1] = vyValues[index1] - 1; vyValues[index2] = vyValues[index2] + 1; }
  else { if (yValues[index1] < yValues[index2]) { vyValues[index1] = vyValues[index1] + 1; vyValues[index2] = vyValues[index2] - 1; } else { } }

  if (zValues[index1] > zValues[index2]) { vzValues[index1] = vzValues[index1] - 1; vzValues[index2] = vzValues[index2] + 1; }
  else { if (zValues[index1] < zValues[index2]) { vzValues[index1] = vzValues[index1] + 1; vzValues[index2] = vzValues[index2] - 1; } else { } }
}

int updatePosition(int index) {
  xValues[index] = xValues[index] + vxValues[index];
  yValues[index] = yValues[index] + vyValues[index];
  zValues[index] = zValues[index] + vzValues[index];
}

int updateTimeStep() {
  updateGravity(0, 1);
  updateGravity(0, 2);
  updateGravity(0, 3);
  updateGravity(1, 2);
  updateGravity(1, 3);
  updateGravity(2, 3);

  updatePosition(0);
  updatePosition(1);
  updatePosition(2);
  updatePosition(3);
}

int potentialEnergy(int index) { return abs(xValues[index]) + abs(yValues[index]) + abs(zValues[index]); }
int kineticEnergy(int index) { return abs(vxValues[index]) + abs(vyValues[index]) + abs(vzValues[index]); }
int moonTotalEnergy(int index) { return potentialEnergy(index) * kineticEnergy(index); }

int repeat_n_times(int n) {
  updateTimeStep();
  if (n > 1) { repeat_n_times(n - 1); } else { }
}

int main() {

    xValues[0] = 0 - 6; xValues[1] = 0; xValues[2] = 0 - 15; xValues[3] = 0 - 3;
    yValues[0] = 0 - 5; yValues[1] = 0 - 3; yValues[2] = 10; yValues[3] = 0 - 8;
    zValues[0] = 0 - 8; zValues[1] = 0 - 13; zValues[2] = 0 - 11; zValues[3] = 3;

    repeat_n_times(1000);

    int totalSystemEnergy = 0;
    totalSystemEnergy = totalSystemEnergy + moonTotalEnergy(0);
    totalSystemEnergy = totalSystemEnergy + moonTotalEnergy(1);
    totalSystemEnergy = totalSystemEnergy + moonTotalEnergy(2);
    totalSystemEnergy = totalSystemEnergy + moonTotalEnergy(3);

    printNumber(totalSystemEnergy);

    return totalSystemEnergy;
}
//expect 49