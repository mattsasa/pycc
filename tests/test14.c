int outside = 2;

int functionWithReturn() {
  return (5 + 3) * 2;
}

int main() {

    int c = 60 - 48;

    int functionOutput = functionWithReturn();

    return functionOutput;
}
//expect 16