
int someFunction(int x) {
  return x + 2;
}

int main() {

    int result = someFunction(someFunction(someFunction(someFunction(1))));

    return result;
}
//expect 9