
int main() {

  char noSizeGivenStr[] = "meow";

  char sizeGivenStr[3] = "abc";

  char curlyCharArray[5] = { 'a', 'b', 'c', 'd', 'e' };

  printf("Testing string types: %s, %s, %s  Done!\n", noSizeGivenStr, sizeGivenStr, curlyCharArray);

  return 0;
}
//print Testing string types: meow, abc, abcde  Done!
//expect 0