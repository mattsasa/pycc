int outside = 2;

int otherFunction(){
  outside = outside + 2;
}

int squareOutside(){
  outside = outside * outside;
}

int main() {

    int c = 60 - 48;

    otherFunction();
    otherFunction();

    squareOutside();

    return outside;
}
//expect 36