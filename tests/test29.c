

int main() {

  int arr[5];
  arr[3] = 8;
  int otherArr[3] = { 4, -4, 9 };

  char str[10] = "HelloWorld";

  char result = arr[3] + otherArr[1] + str[9];

  return result;
}
//expect 104