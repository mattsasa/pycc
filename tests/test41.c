int main() {

    int x = 24;

    if ((x % 3) == 0 && (x % 4) == 0) {
      return 3;
    } else {

      if ((x % 3) == 0) {
        return 2;
      } else { }

      if ((x % 4) == 0) {
        return 1;
      } else { }

    }

    return 0;
}
//expect 3