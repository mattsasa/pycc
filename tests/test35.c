int genNumber() {
  return 1234;
}


int main() {

  int aNumber = -456;

  char noSizeGivenStr[] = "meow";

  char sizeGivenStr[3] = "abc";

  char curlyCharArray[5] = { 'a', 'b', 'c', 'd', 'e' };

  printf("Testing combination types: %d, %d, %d, %d, %s, %s, %s  Done!\n", 4, aNumber, 5*-80, genNumber(), noSizeGivenStr, sizeGivenStr, curlyCharArray);

  return 0;
}
//print Testing combination types: 00004, -00456, -00400, 01234, meow, abc, abcde  Done!
//expect 0