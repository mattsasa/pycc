
int someFunction(int x, int z, int y) {
  return (x - y) * z;
}

int main() {

    int result = someFunction(5, 8, 3);

    return result;
}
//expect 16