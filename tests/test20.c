int global;

int increaseGlobal(int x) {
  global = global + x;
}

int main() {

    global = 3;

    increaseGlobal(6);

    return global;
}
//expect 9