int main() {

    bool b = true || (true && false);

    if (b) {
      printf("evaluated to true\n");
      return 1;
    } else {
      printf("evaluated to false\n");
      return 0;
    }

    return 5;
}
//expect 1