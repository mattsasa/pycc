int abs(int num) {
  if (num < 0) { return num * -1; }
  else { return num; }
}

int xValues[4] = { -6, 0, -15, -3 };
int yValues[4] = { -5, -3, 10, -8 };
int zValues[4] = { -8, -13, -11, 3 };
int vxValues[4];
int vyValues[4];
int vzValues[4];

int updateGravity(int index1, int index2) {

  if (xValues[index1] > xValues[index2]) { vxValues[index1]--; vxValues[index2]++; }
  else { if (xValues[index1] < xValues[index2]) { vxValues[index1]++; vxValues[index2]--; } }

  if (yValues[index1] > yValues[index2]) { vyValues[index1]--; vyValues[index2]++; }
  else { if (yValues[index1] < yValues[index2]) { vyValues[index1]++; vyValues[index2]--; } }

  if (zValues[index1] > zValues[index2]) { vzValues[index1]--; vzValues[index2]++; }
  else { if (zValues[index1] < zValues[index2]) { vzValues[index1]++; vzValues[index2]--; } }
}

int updatePosition(int index) {
  xValues[index] += vxValues[index];
  yValues[index] += vyValues[index];
  zValues[index] += vzValues[index];
}

int updateTimeStep() {
  updateGravity(0, 1);
  updateGravity(0, 2);
  updateGravity(0, 3);
  updateGravity(1, 2);
  updateGravity(1, 3);
  updateGravity(2, 3);

  for (int i = 0; i < 4; i++) { updatePosition(i); }

}

int potentialEnergy(int index) { return abs(xValues[index]) + abs(yValues[index]) + abs(zValues[index]); }
int kineticEnergy(int index) { return abs(vxValues[index]) + abs(vyValues[index]) + abs(vzValues[index]); }
int moonTotalEnergy(int index) { return potentialEnergy(index) * kineticEnergy(index); }

int main() {

    for (int i = 0; i < 1000; i+=1) { updateTimeStep(); }

    int totalSystemEnergy = 0;
    totalSystemEnergy += moonTotalEnergy(0);
    totalSystemEnergy += moonTotalEnergy(1);
    totalSystemEnergy += moonTotalEnergy(2);
    totalSystemEnergy += moonTotalEnergy(3);

    printf("Total System Energy: %d\n", totalSystemEnergy);

    return totalSystemEnergy;
}
//expect 49