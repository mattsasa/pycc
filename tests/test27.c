

int main() {

    int x = 5;
    int8 y = 3;
    int8 a = 255+1;
    int8 z = a*3 + 9;

    char c = 4 + 9;
    char str[5] = "apple";
    char greeting[5] = {'H', 'e', 'l', 'l', 'o'};
    c = 'c';
    greeting[3] = 'a';

    return greeting[1] + str[0];
}
//expect 198