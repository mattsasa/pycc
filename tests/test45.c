int main() {

    int number = 0;
    for (int i = 4; i < 50; i *= 2) {
      printf("i: %d\n", i);
      number = i;
    }

    printf("Done \n");

    return number;
}
//expect 32