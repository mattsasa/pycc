int main() {

    int foo = 8 * 2;
    int bar = foo - 7;
    int meow = (foo - 2) * (foo - 3);

    return (meow - (9 + foo)) + bar * 8;
}
//expect 229