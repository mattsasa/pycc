#!/usr/bin/expect -f

set program_name [lindex $argv 0]

set input [lindex $argv 1]

set timeout -1

spawn "$program_name"

expect {
	-re "" {
		send "${input}\r"
	}
}
expect eof
catch wait result
exit [lindex $result 3]
