#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
WHITE='\033[0;37m'

totaltests=0
passedtests=0

mkdir -p elfs

for filename in tests/*
do
  let totaltests++
  python3 pycc.py $filename &> /dev/null
  if [ $? -eq 0 ]
  then
    echo -e "${GREEN}Test: $filename compile succeeded"
    let passedtests++
  else
    echo -e "${RED}Test: $filename compile failed"
  fi
done

echo -e "${WHITE}$passedtests out of $totaltests tests compiled!"
