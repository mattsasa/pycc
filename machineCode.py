from util import toByteArray, BasicObject, throwError, throwWarning
from constants import *

class MachineCode:
    def __init__(self):
        self.programBytes = []
        self.dataBytes = []

        self.programIndex = 0
        self.dataIndex = 0
        self.bssIndex = 278 # used to be 270...  needed to move to 278

    def addDWordToProgram(self, int):
        if int < 0: int += 4294967296
        self.programBytes.extend(toByteArray(int))
        self.programIndex += 4

    def addDWordToData(self, int):
        if int < 0: int += 4294967296
        self.dataBytes.extend(toByteArray(int))
        self.dataIndex += 4

    def addByteToProgram(self, byte):
        if byte < 0: byte += 256
        if byte > 255:
            throwError('error - program byte must be only 1 byte')
        self.programBytes.append(byte)
        self.programIndex += 1

    def addByteToData(self, byte):
        if byte < 0: byte += 256
        if byte > 255:
            throwError('error - data byte must be only 1 byte')
        self.dataBytes.append(byte)
        self.dataIndex += 1

    def updateProgramAtIndex(self, newBytes, start, length = 4):
        self.programBytes[start:start+length] = newBytes

    def movEAXtoMemory(self, address):
        self.addByteToProgram(0xA3)
        self.addDWordToProgram(address)

    def movALtoMemory(self, address):
        self.addByteToProgram(0xA2)
        self.addDWordToProgram(address)

    def movMemoryToEAX(self, address):
        self.addByteToProgram(0xA1)
        self.addDWordToProgram(address)

    def movMemoryToAL(self, address):
        self.addByteToProgram(0xA0)
        self.addDWordToProgram(address)

    def movRegAtoMemory(self, dataSize, address):
        self.movEAXtoMemory(address) if dataSize == 4 else self.movALtoMemory(address)

    def movMemoryToRegA(self, dataSize, address):
        self.movMemoryToEAX(address) if dataSize == 4 else self.movMemoryToAL(address)

    def addReturn(self):
        if self.programBytes[self.programIndex - 1] != 0xC3: self.addByteToProgram(0xC3) #ret

    def resetEAX(self):
        self.addByteToProgram(0x31) #xor eax, eax
        self.addByteToProgram(0xC0)

    def resetEDX(self):
        self.addByteToProgram(0x31) #xor edx, edx
        self.addByteToProgram(0xD2)

    def swapECXEAX(self):
        self.addByteToProgram(0x91)

    def testEaxEqualZero(self):
        self.addByteToProgram(0x85)
        self.addByteToProgram(0xC0)

    def compareEAXtoECX(self):
        self.addByteToProgram(0x39)
        self.addByteToProgram(0xC8)

    def conditionalJump(self, comparison, offset):
        self.addByteToProgram(0x0F)
        self.addByteToProgram(comparisonToJumpOpCode[comparison])
        self.addDWordToProgram(offset)

    def unconditionalJump(self, offset):
        self.addByteToProgram(0xE9)
        self.addDWordToProgram(offset)

    def movValueAtEBXaddressToRegA(self, dataSize):
        if dataSize == 4: self.movValueAtEBXaddressToEAX()
        elif dataSize == 1: self.movValueAtEBXaddressToAL()
        else: throwError('Error in movValueAtEBXaddressToRegA - invalid dataSize')

    def moveVarInStackToEAX(self, stackOffset):
        self.movESPtoEBX() #mov ebx, esp
        self.addImmediateToBL(stackOffset) #add ebx, stackOffset
        self.movValueAtEBXaddressToRegA(4) #mov eax, [ebx] -- variable size

    def movESPtoEBX(self):
        self.addByteToProgram(0x89) #mov ebx, esp
        self.addByteToProgram(0xE3)

    def mulEBX(self):
        self.addByteToProgram(0xF7)
        self.addByteToProgram(0xE3)

    def mulDWordAtAddress(self, address):
        self.addByteToProgram(0xF7) #mul dword [var] (into eax)
        self.addByteToProgram(0x25)
        self.addDWordToProgram(address)

    def divDWordAtAddress(self, address):
        self.resetEDX()
        self.addByteToProgram(0xF7) #div dword [var] (quotient left in eax, remainder left in edx)
        self.addByteToProgram(0x35)
        self.addDWordToProgram(address)

    def orDWordAtAddressToEAX(self, address):
        self.addByteToProgram(0x0B) #or eax, [var]
        self.addByteToProgram(0x05)
        self.addDWordToProgram(address)

    def andDWordAtAddressToEAX(self, address):
        self.addByteToProgram(0x23) #and eax, [var]
        self.addByteToProgram(0x05)
        self.addDWordToProgram(address)

    def addDWordAtAddressToEAX(self, address):
        self.addByteToProgram(0x03) #add eax, [var]
        self.addByteToProgram(0x05)
        self.addDWordToProgram(address)

    def subDWordAtAddressToEAX(self, address):
        self.addByteToProgram(0x2B) #sub eax, [var]
        self.addByteToProgram(0x05)
        self.addDWordToProgram(address)

    def divEBX(self):
        self.resetEDX()
        self.addByteToProgram(0xF7) #div ebx (quotient left in eax, remainder in edx)
        self.addByteToProgram(0xF3)

    def orECXtoEAX(self):
        self.addByteToProgram(0x09) #or eax, ecx
        self.addByteToProgram(0xC8)

    def andECXtoEAX(self):
        self.addByteToProgram(0x21) #and eax, ecx
        self.addByteToProgram(0xC8)

    def addECXtoEAX(self):
        self.addByteToProgram(0x01) #add eax, ecx
        self.addByteToProgram(0xC8)

    def subECXfromEAX(self):
        self.addByteToProgram(0x29) #sub eax, ecx
        self.addByteToProgram(0xC8)

    def orImmediateToEAX(self, immediate):
        self.addByteToProgram(0x0D)
        self.addDWordToProgram(int(immediate))

    def andImmediateToEAX(self, immediate):
        self.addByteToProgram(0x25)
        self.addDWordToProgram(int(immediate))

    def addImmediateToEAX(self, immediate):
        self.addByteToProgram(0x05)
        self.addDWordToProgram(int(immediate))

    def addImmediateToBL(self, immediate):
        self.addByteToProgram(0x83)
        self.addByteToProgram(0xC3)
        self.addByteToProgram(int(immediate))

    def subImmediateToEAX(self, immediate):
        self.addByteToProgram(0x2D)
        self.addDWordToProgram(int(immediate))

    def movECXtoAddressOfEAX(self): ## mov [eax], ecx
        self.addByteToProgram(0x89)
        self.addByteToProgram(0x08)

    def movValueAtEAXaddressToRegA(self, dataSize):
        if dataSize ==  4:  self.movValueAtEAXaddressToEAX()
        elif dataSize == 1: self.movValueAtEAXaddressToAL()
        else: throwError('Error - invalid dataSize in movValueAtEAXaddressToRegA')

    def movValueAtEAXaddressToEAX(self): ## mov eax, [eax]
        self.addByteToProgram(0x8B)
        self.addByteToProgram(0x00)

    def movValueAtEAXaddressToAL(self): ## mov al, [eax]
        self.addByteToProgram(0x8A)
        self.addByteToProgram(0x00)
        self.andImmediateToEAX(255) ##clear upper registers leaving only data in AL

    def movValueAtEBXaddressToAL(self): ## mov al, [ebx]
        ### possibly need to reset EAX to clear out upper registers
        self.resetEAX()
        self.addByteToProgram(0x8A)
        self.addByteToProgram(0x03)

    def movValueAtEBXaddressToEAX(self): ## mov eax, [ebx]
        self.addByteToProgram(0x8B)
        self.addByteToProgram(0x03)

    def movEAXtoEBX(self): #mov ebx, eax
        self.addByteToProgram(0x89)
        self.addByteToProgram(0xC3)

    def movECXtoEBX(self): #mov ebx, ecx
        self.addByteToProgram(0x89)
        self.addByteToProgram(0xCB)

    def movImmediateToEAX(self, immediate):
        self.addByteToProgram(0xB8) #mov  eax, immediate
        self.addDWordToProgram(int(immediate))

    def movImmediateToEBX(self, immediate):
        self.addByteToProgram(0xBB) #mov  ebx, immediate
        self.addDWordToProgram(int(immediate))

    def movImmediateToECX(self, immediate):
        self.addByteToProgram(0xB9) #mov  ecx, immediate
        self.addDWordToProgram(int(immediate))

    def movImmediateToEDX(self, immediate):
        self.addByteToProgram(0xBA) #mov  edx, immediate
        self.addDWordToProgram(int(immediate))

    def copyRemainderToEAX(self):
        self.addByteToProgram(0x89) #mov eax, edx
        self.addByteToProgram(0xD0)

    def copyFlagsToEAX(self):
        self.pushFlags()
        self.popEAX()

    def pushFlags(self): self.addByteToProgram(0x9C)

    def pushEAX(self): self.addByteToProgram(0x50)

    def popEAX(self): self.addByteToProgram(0x58)

    def popECX(self): self.addByteToProgram(0x59)

    def callRelative(self, offset):
        self.addByteToProgram(0xE8) #call offset
        self.addDWordToProgram(offset)

    def setEAXtoSysRead(self):
        self.addByteToProgram(0xB8)
        self.addDWordToProgram(3)

    def setEAXtoSysWrite(self):
        self.addByteToProgram(0xB8)
        self.addDWordToProgram(4)

    def setEAXtoSysExit(self):
        self.addByteToProgram(0xB8)
        self.addDWordToProgram(1)

    def setEBXtoStdin(self):
        self.addByteToProgram(0xBB)
        self.addDWordToProgram(0)

    def setEBXtoStdout(self):
        self.addByteToProgram(0xBB)
        self.addDWordToProgram(1)

    def callKernel(self):
        self.addByteToProgram(0xCD)
        self.addByteToProgram(0x80)

    def writeCodeForSysWrite(self, length, address):
        self.movImmediateToEDX(length) ## mov EDX, length  -- length of message to write
        self.movImmediateToECX(address) # mov ECX, address  (address of variable / message)
        self.setEBXtoStdout()
        self.setEAXtoSysWrite()
        self.callKernel()

    def takeProgramBytesFrom(self, index):
        bytes = self.programBytes[index:self.programIndex]
        self.programBytes = self.programBytes[:index]
        self.programIndex = index
        return bytes

    def addByteArrayToProgram(self, array):
        self.programIndex += len(array)
        self.programBytes.extend(array)

    def getFinalOutput(self):
        return self.programBytes, self.dataBytes, self.bssIndex

