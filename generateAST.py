import sys
from pptree import *
from util import BasicObject

class NodeAST:
    def __init__(self, type):
        self.type = type

##### Grammar
# function:
# 	TYPE IDENTIFIER '(' ')' '{' [statement] '}'
#
# statement:
#   returnStatement || declarationStatement || ifStatement || expression ';'
#
# returnStatement:
#   RETURN expression ';'
#
# declarationStatement:
#   TYPE IDENTIFIER '=' expression  ';'
#
# comparisonExpression:
#   expression COMPARISON expression
#
# factor:
#   NUMBER || STRING || functionCall || IDENTIFIER || '(' expression ')'
#
# expression:
#    additive_operator_expression || term || comparisonExpression
#
# term:
#   multiplicative_operator_expression || factor
#
# additive_operator_expression:
#   term ADDITIVE_OPERATOR term || term ADDITIVE_OPERATOR additive_operator_expression
#
# multiplicative_operator_expression:
#   factor MULTIPLICATIVE_OPERATOR factor || factor MULTIPLICATIVE_OPERATOR multiplicative_operator_expression
#
# functionCall:
#   IDENTIFIER '(' [expression]  ')'


class ParserAST:
    def __init__(self, tokens):
        self.tokens = tokens
        self.tokenIndex = 0

    def parseError(self, msg):
        print("Error Parsing AST Tree")
        print(msg)
        sys.exit(1)

    def currentToken(self):
        return self.tokens[self.tokenIndex]

    def nextToken(self):
        return self.tokens[self.tokenIndex + 1]

    def takeToken(self):
        token = self.currentToken()
        self.tokenIndex += 1
        return token

    def eat(self, value):
        if self.currentToken().value != value: self.parseError('Eat invalid - expected ' + value)
        self.takeToken()

    def terminatingNode(self, type):
        if self.currentToken().type != type:
            self.parseError("terminatingNode invalid - expected: " + type + " recieved: " + self.currentToken().type)
        node = NodeAST('terminating')
        node.token = self.takeToken()
        return node

    def returnStatementNode(self):
        node = NodeAST('return statement')
        self.eat('return')
        node.returnExpression = self.expressionNode()
        self.eat(';')
        return node

    def declarationStatementNode(self):
        node = NodeAST('declaration statement')
        node.variableTypeNode = self.terminatingNode('Type')
        node.variableNameNode = self.terminatingNode('Identifer')
        if self.currentToken().value == '[':
            node.variableNameNode = self.arrayCall(node.variableNameNode) #arrayName is argument
        if self.currentToken().value == '=':
            self.eat('=')
            if self.currentToken().value == '{':  #declare array with '{'
                node.arrayInitValues = []
                self.eat('{')
                while (self.currentToken().value != '}'):
                    node.arrayInitValues.append(self.terminatingNode('Constant'))
                    if self.currentToken().value == ',': self.eat(',')
                self.eat('}')
            else:  #declare expression or value  -- normal case
                node.variableInitValue = self.expressionNode()
        self.eat(';')
        return node

    def functionArgument(self):
        node = NodeAST('function argument')
        node.variableTypeNode = self.terminatingNode('Type')
        node.variableNameNode = self.terminatingNode('Identifer')
        return node

    def assignmentStatementNode(self, eatSemiColon):
        node = NodeAST('assignment statement')
        startIndex = self.tokenIndex
        node.variableNameNode = self.terminatingNode('Identifer')
        if self.currentToken().value == '[':
            node.variableNameNode = self.arrayCall(node.variableNameNode) #arrayName is argument
        endIndex = self.tokenIndex

        if (self.currentToken().type == 'Assignment Operator'): ## converting tokens to regular assignment
            operator = self.currentToken().value[0]
            self.terminatingNode('Assignment Operator')
            shift = startIndex + (endIndex - startIndex) + 1
            for i in range(startIndex, endIndex):
                self.tokens.insert(shift, self.tokens[i])
                shift += 1
            newOperatorToken = BasicObject()
            newOperatorToken.type = 'Operator'
            newOperatorToken.value = operator
            self.tokens.insert(shift, newOperatorToken)
        else:
            self.eat('=')

        node.variableInitValue = self.expressionNode()
        if eatSemiColon: self.eat(';')
        return node

    def whileLoopNode(self):
        node = NodeAST('while loop')
        self.eat('while')
        self.eat('(')
        node.condition = self.expressionNode()
        self.eat(')')
        self.eat('{')
        node.body = self.statementListNode()
        self.eat('}')
        return node

    def forLoopNode(self):
        node = NodeAST('for loop')
        self.eat('for')
        self.eat('(')
        node.initalization = self.declarationStatementNode()
        node.condition = self.expressionNode()
        self.eat(';')
        node.increment = self.assignmentStatementNode(False)
        self.eat(')')
        self.eat('{')
        node.body = self.statementListNode()
        self.eat('}')
        return node

    def ifStatementNode(self):
        node = NodeAST('if statement')
        self.eat('if')
        self.eat('(')
        node.condition = self.expressionNode()
        self.eat(')')
        self.eat('{')
        node.ifbody = self.statementListNode()
        self.eat('}')
        if self.currentToken().value == 'else':
            self.eat('else')
            self.eat('{')
            node.elsebody = self.statementListNode()
            self.eat('}')
        else:
            node.elsebody = NodeAST('statement list')
            node.elsebody.statements = []
        return node

    def makeNewStatementNode(self):
        if self.currentToken().value == 'for':
            return self.forLoopNode()
        elif self.currentToken().value == 'while':
            return self.whileLoopNode()
        elif self.currentToken().value == 'return': ## return
            return self.returnStatementNode()
        elif self.currentToken().value == 'if': ## if
            return self.ifStatementNode()
        elif self.currentToken().type == 'Type': ## declaration
            if self.tokens[self.tokenIndex + 2].value ==  '(':
                return self.functionNode()
            else:
                return self.declarationStatementNode()
        elif self.nextToken().type == 'Assignment Operator' or self.nextToken().value == '=' or self.nextToken().value == '[': ## assignment
            return self.assignmentStatementNode(True)
        else:
            expression = self.expressionNode()
            self.eat(';')
            return expression

    def statementListNode(self):
        node = NodeAST('statement list')
        node.statements = []
        while self.tokenIndex < len(self.tokens) and self.currentToken().value != '}':
            node.statements.append(self.makeNewStatementNode())
        return node

    def factorNode(self):
        node = NodeAST('factor')
        if self.currentToken().value in ('true','false'):
            node = self.terminatingNode('Construct')
        elif self.currentToken().type == 'Constant':
            node = self.terminatingNode('Constant')
        elif self.currentToken().type == 'Identifer':
            node = self.terminatingNode('Identifer')
            if self.currentToken().value == '(':
                node = self.functionCallNode(node) #functionName is argument
            if self.currentToken().value == '[':
                node = self.arrayCall(node) #arrayName is argument
        elif self.currentToken().value == '(':  ## parenthesis recursion expression
            self.eat('(')
            node = self.expressionNode()
            self.eat(')')
        else: self.parseError('factor')

        if self.currentToken().type == 'Comparison':
            node = self.comparisonExpressionNode(node)

        return node


    def expressionNode(self):
        node = NodeAST('expression')
        node = self.termNode()

        if self.currentToken().value in ('+','-'):
            node = self.additiveOperatorExpression(node)

        return node

    def comparisonExpressionNode(self, left):
        node = NodeAST('comparison expression')
        node.leftSide = left
        node.comparison = self.terminatingNode('Comparison')
        node.rightSide = self.factorNode()

        if self.currentToken().value in ('&&','||'):
            node = self.booleanExpression(node)

        return node

    def booleanExpression(self, left):
        node = NodeAST('boolean expression')
        node.leftOperand = left
        node.operator = self.terminatingNode('Operator')
        node.rightOperand = self.factorNode()

        if self.currentToken().value in ('&&','||'):
            return self.booleanExpression(node)

        return node

    def additiveOperatorExpression(self, left):
        node = NodeAST('operator expression')
        node.leftOperand = left
        node.operator = self.terminatingNode('Operator')
        node.rightOperand = self.termNode()

        if self.currentToken().value in ('+','-'):
            return self.additiveOperatorExpression(node)

        return node

    def multiplicativeOperatorExpression(self, left):
        node = NodeAST('operator expression')
        node.leftOperand = left
        node.operator = self.terminatingNode('Operator')
        node.rightOperand = self.factorNode()

        if self.currentToken().value in ('*','/','%'):
            return self.multiplicativeOperatorExpression(node)

        return node

    def termNode(self):
        node = NodeAST('term')
        node = self.factorNode()

        if self.currentToken().value in ('*','/','%'):
            node = self.multiplicativeOperatorExpression(node)

        if self.currentToken().value in ('&&','||'):
            node = self.booleanExpression(node)

        return node

    def arrayCall(self, arrayName):
        node = NodeAST('array call')
        node.arrayName = arrayName
        self.eat('[')
        if self.currentToken().value == ']': node.arrayIndex = None
        else: node.arrayIndex = self.expressionNode()
        self.eat(']')
        return node

    def functionCallNode(self, functionName):
        node = NodeAST('function call')
        node.functionName = functionName
        node.arguments = []
        self.eat('(')
        while (self.currentToken().value != ')'):
            node.arguments.append(self.expressionNode())
            if self.currentToken().value == ',': self.eat(',')
        self.eat(')')
        return node

    def functionNode(self):
        node = NodeAST('function')
        node.functionOutputTypeNode = self.terminatingNode('Type')
        node.functionName = self.terminatingNode('Identifer')
        node.arguments = []
        self.eat('(')
        while (self.currentToken().value != ')'):
            node.arguments.append(self.functionArgument())
            if self.currentToken().value == ',': self.eat(',')
        self.eat(')')
        self.eat('{')
        node.functionBodyNode = self.statementListNode()
        self.eat('}')
        return node

    def parse(self):
        return self.statementListNode()

def createTreePP(asTree, parent):
    if asTree.type == 'terminating':
        value = Node(asTree.token.print() + " value", parent)
        return

    root = Node(asTree.type) if parent == None else Node(asTree.type, parent)

    if root.name == 'statement list':
        for statement in asTree.statements:
            createTreePP(statement, root)
    if root.name == 'comparison expression':
        createTreePP(asTree.leftSide, root)
        comparison = Node(asTree.comparison.token.print() + " comparison", root)
        createTreePP(asTree.rightSide, root)
    if root.name == 'operator expression':
        createTreePP(asTree.leftOperand, root)
        operator = Node(asTree.operator.token.print() + " operator", root)
        createTreePP(asTree.rightOperand, root)
    if root.name == 'if statement':
        condition = Node('condition', root)
        ifbody = Node('if body', root)
        elsebody = Node('else body', root)
        createTreePP(asTree.condition, condition)
        createTreePP(asTree.ifbody, ifbody)
        createTreePP(asTree.elsebody, elsebody)
    if root.name == 'return statement':
        createTreePP(asTree.returnExpression, root)
    if root.name == 'array call':
        arrayName = Node(asTree.arrayName.token.print() + " arrayName", root)
        createTreePP(asTree.arrayIndex, root)
    if root.name == 'function call':
        functionName = Node(asTree.functionName.token.print() + " functionName", root)
        for arg in asTree.arguments:
            createTreePP(arg, root)
    if root.name == 'declaration statement' or root.name == 'assignment statement':
        if hasattr(asTree, 'variableTypeNode'):
            variableTypeNode = Node(asTree.variableTypeNode.token.print() + " variableType", root)
        createTreePP(asTree.variableNameNode, root)
        if hasattr(asTree, 'variableInitValue'):
            createTreePP(asTree.variableInitValue, root)
    if root.name == 'function':
        functionOutputTypeNode = Node(asTree.functionOutputTypeNode.token.print() + " functionOutputType", root)
        functionName = Node(asTree.functionName.token.print() + " functionName", root)
        createTreePP(asTree.functionBodyNode, root)
    return root

def generate_ast(tokens):
    parser = ParserAST(tokens)
    outputTree = parser.parse()
    # print("finished parsing")
    # print_tree(createTreePP(outputTree, None))
    return outputTree
