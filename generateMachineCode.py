from util import *
from constants import *
from systemFunctions import SystemFunctions
from machineCode import MachineCode
from variablesHandler import VariablesHandler
from functionsHandler import FunctionsHandler

class Generator:
    def __init__(self):
        self.code = MachineCode()
        self.functions = FunctionsHandler()
        self.vars = VariablesHandler()
        self.sysFuctions = SystemFunctions(self, self.code)

    def handleArrayAssignment(self, statement):
        self.putExpressionIntoEax(statement.variableInitValue)
        var = self.vars.getVar(statement.variableNameNode.arrayName.token.value)
        self.code.pushEAX()
        self.putExpressionIntoEax(statement.variableNameNode.arrayIndex) ## mov eax, arrayIndexResult
        self.code.movImmediateToEBX(var.dataSize)    # mov EBX, 4  (datasize)
        self.code.mulEBX()                           # MUL EBX (with EAX into EAX)
        self.code.addImmediateToEAX(var.memLocation) # add eax, immediate (arrayStartAddr)
        self.code.popECX()                           # pop ecx
        self.code.movECXtoAddressOfEAX()             # mov [eax], ecx

    def handleVariableAssignment(self, statement):
        ### Optional add optimization for when rhs is immediate value
        if statement.variableNameNode.type == 'array call':
            self.handleArrayAssignment(statement)
        else:
            self.putExpressionIntoEax(statement.variableInitValue)
            varName = statement.variableNameNode.token.value
            self.vars.updateVarFromEaxValue(varName, self.code)

    def handleArrayDeclaration(self, statement, type, varName):  ### refactor
        if statement.variableNameNode.arrayIndex == None:
            if hasattr(statement, 'arrayInitValues'):
                arraySize = len(statement.arrayInitValues)
            elif hasattr(statement, 'variableInitValue') and type == 'char':
                arraySize = len(statement.variableInitValue.token.value) - 2
            else: throwError('Error cant get arraySize')
        elif statement.variableNameNode.arrayIndex.token.type != 'Constant':
            throwError('Error: Non immediate array size declarations not supported')
        else:
            arraySize = int(statement.variableNameNode.arrayIndex.token.value)
        if hasattr(statement, 'arrayInitValues'):
            newVar = self.vars.createDataVar(varName, statement.arrayInitValues[0].token.value, type, arraySize, self.code)
            for element in statement.arrayInitValues[1:]:
                if type == 'int':
                    if not element.token.value.lstrip('-').isnumeric(): throwError('int types must be numbers')
                    self.code.addDWordToData(int(element.token.value))
                elif type == 'int8':
                    if not element.token.value.lstrip('-').isnumeric(): throwError('int types must be numbers')
                    self.code.addByteToData(int(element.token.value))
                elif type == 'char':
                    if len(element.token.value) != 3: throwError('Error - char should 1 character surronded with quotes')
                    self.code.addByteToData(ord(element.token.value[1]))
        elif hasattr(statement, 'variableInitValue'):  ## string style char[] init
            value = statement.variableInitValue.token.value
            newVar = self.vars.createDataVar(varName, "'" + value[1] + "'", type, arraySize, self.code)
            if len(value) > 2:
                for i in range(2,len(value)-1):
                    self.code.addByteToData(ord(value[i]))
            else: throwError('Error - char[] init')
        else:
            newVar = self.vars.createBssVar(varName, type, self.code)
            self.code.bssIndex += (typeDataSizeTable[type] * (arraySize - 1))
        newVar.arraySize = arraySize

    def handleVariableDeclaration(self, statement):
        type = statement.variableTypeNode.token.value
        if statement.variableNameNode.type == 'array call':
            varName = statement.variableNameNode.arrayName.token.value
        else:
            varName = statement.variableNameNode.token.value

        currentVarsWithName = []
        for vars in self.vars.scopes:
            currentVarsWithName.extend(list(filter(lambda v: v.name == varName, vars)))
        if len(currentVarsWithName) > 0:
            throwError('error duplicate variable declaration: ' + varName)
        if type not in ('int','int8','char','bool'):
            throwError('Error - umimplemented dataType declaration')

        if not statement.variableNameNode.type == 'array call' and hasattr(statement, 'arrayInitValues'):
            throwError('Array declaration error - ' + str(varName) + ' - array values exist without arraytype declaration')

        if statement.variableNameNode.type == 'array call':
            self.handleArrayDeclaration(statement, type, varName)
        elif not hasattr(statement, 'variableInitValue'):
            self.vars.createBssVar(varName, type, self.code)
        elif hasattr(statement.variableInitValue, 'token') and statement.variableInitValue.token.type == 'Constant':
            self.vars.createDataVar(varName, statement.variableInitValue.token.value, type, 1, self.code)
        else:   ## declaring variable and initalizing to an expression
            self.putExpressionIntoEax(statement.variableInitValue)
            self.vars.createBssVarFromEaxValue(varName, type, self.code)


    def handleOperationExpression(self, expression):
        if not hasattr(expression, 'operator'):  ### A comparison operation
            self.handleComparisonStatement(expression)
            return

        operator = expression.operator.token.value

        ## put leftOperand into EAX
        self.putExpressionIntoEax(expression.leftOperand)

        ## perform operation with rightOperand to EAX
        if hasattr(expression.rightOperand, 'token'):   ### shortcuts for more efficent code
            if expression.rightOperand.token.type == 'Construct':
                if expression.rightOperand.token.value == 'true': self.code.movImmediateToECX(1)
                elif expression.rightOperand.token.value == 'false': self.code.movImmediateToECX(0)
                else: throwError('Only constructs should be true or false')
                self.handleOperationWithECX(operator)
            elif expression.rightOperand.token.type == 'Constant':
                self.handleOperationWithImmediate(expression.rightOperand.token, operator)
            elif expression.rightOperand.token.type == 'Identifer':
                self.handleOperationWithVariable(expression.rightOperand.token, operator)
            else:
                throwError('error - handleOperationExpression - rightOperand unknown')
        else:
            self.code.pushEAX()
            self.putExpressionIntoEax(expression.rightOperand)
            self.code.popECX()
            self.code.swapECXEAX()
            self.handleOperationWithECX(operator)


    def handleOperationWithECX(self, operator):

        if operator in ('*','/','%'): self.code.movECXtoEBX()

        if operator == '*': self.code.mulEBX()
        elif operator == '+': self.code.addECXtoEAX()
        elif operator == '-': self.code.subECXfromEAX()
        elif operator in ('/','%'): self.code.divEBX() #div ebx (quotient left in eax)
        elif operator == '&&': self.code.andECXtoEAX()
        elif operator == '||': self.code.orECXtoEAX()
        else: throwError('unimplemented variable operation')

        self.postOperations(operator)

    def handleOperationWithVariable(self, token, operator):
        varAddr = self.vars.getVar(token.value).memLocation

        if operator == '*': self.code.mulDWordAtAddress(varAddr)
        elif operator == '+': self.code.addDWordAtAddressToEAX(varAddr)
        elif operator == '-': self.code.subDWordAtAddressToEAX(varAddr)
        elif operator in ('/','%'): self.code.divDWordAtAddress(varAddr)
        elif operator == '&&': self.code.andDWordAtAddressToEAX(varAddr)
        elif operator == '||': self.code.orDWordAtAddressToEAX(varAddr)
        else: throwError('unimplemented variable operation')

        self.postOperations(operator)

    def handleOperationWithImmediate(self, token, operator):
        if operator in ('*','/','%'): self.code.movImmediateToEBX(token.value)

        if operator == '*': self.code.mulEBX()
        elif operator in ('/','%'): self.code.divEBX() #div ebx (quotient left in eax)
        elif operator == '+': self.code.addImmediateToEAX(token.value)
        elif operator == '-': self.code.subImmediateToEAX(token.value)
        elif operator == '&&': self.code.andImmediateToEAX(token.value)
        elif operator == '||': self.code.orImmediateToEAX(token.value)
        else: throwError('unimplemented immediate operation')

        self.postOperations(operator)

    def postOperations(self, operator):
        if operator == '%': self.code.copyRemainderToEAX()
        if operator in ('||','&&'): self.code.andImmediateToEAX(1) ## clears all bits except last

    def putExpressionIntoEax(self, expression):

        if expression.type in ('operator expression', 'boolean expression', 'comparison expression'):
            self.handleOperationExpression(expression)
        elif expression.type == 'function call':
            self.handleFunctionCall(expression)
        elif expression.type == 'array call':
            self.handleArrayCall(expression)
        elif expression.token.value in ('true','false'):
            self.code.movImmediateToEAX(1) if expression.token.value == 'true' else self.code.movImmediateToEAX(0)
        elif expression.token.type == 'Identifer':
            var = self.vars.getVar(expression.token.value)
            self.code.movMemoryToRegA(var.dataSize, var.memLocation)
        elif expression.token.type == 'Constant':
            if expression.token.value[0] in ("'",'"'):
                if len(expression.token.value) != 3: throwError('Error - char should 1 character surronded with quotes')
                self.code.movImmediateToEAX(ord(expression.token.value[1]))
            else:
                if not expression.token.value.lstrip('-').isnumeric(): throwError('int types must be numbers')
                self.code.movImmediateToEAX(int(expression.token.value))
        else:
            throwError('error unknown move expression into EAX')


    def handleArrayCall(self, arrayNode):  ## Leaves result in EAX
        array = self.vars.getVar(arrayNode.arrayName.token.value)
        self.putExpressionIntoEax(arrayNode.arrayIndex)
        self.code.movImmediateToEBX(array.dataSize) # mov EBX, arraySize
        self.code.mulEBX() ## MUL EBX (with EAX into EAX)
        self.code.addImmediateToEAX(array.memLocation) ## ADD EAX, arrayStartAddr
        self.code.movValueAtEAXaddressToRegA(array.dataSize) # mov regA, [eax]

    def handleComparisonStatement(self, statement):
        self.putExpressionIntoEax(statement.leftSide)
        self.code.pushEAX() ## Push EAX -- storing
        self.putExpressionIntoEax(statement.rightSide)
        self.code.popECX() ## POP ECX
        self.code.compareEAXtoECX()
        self.code.conditionalJump(statement.comparison.token.value, 10) ## short jump
        self.code.movImmediateToEAX(1)
        self.code.unconditionalJump(5) #clear the last set EAX
        self.code.movImmediateToEAX(0)


    def handleIfStatement(self, statement):
        self.putExpressionIntoEax(statement.condition)
        self.code.testEaxEqualZero()
        self.code.conditionalJump('!=', 0xFFFFFFFF) ## jump if equal to zero (false) (skipping to else block)
        jumpDistanceIndex = self.code.programIndex

        ## specify how far to jump with a signed dword: ifbody code length + 5 (jumping to start of elsebody)
        ifbodylength = self.handleStatementList(statement.ifbody.statements)
        self.code.updateProgramAtIndex(toByteArray(ifbodylength + 5), jumpDistanceIndex - 4)

        ## jump no matter what with a signed dword: elsebody code length (jumping to end of elsebody)
        self.code.unconditionalJump(0xFFFFFFFF)
        jumpDistanceIndex = self.code.programIndex
        elsebodylength = self.handleStatementList(statement.elsebody.statements)
        self.code.updateProgramAtIndex(toByteArray(elsebodylength), jumpDistanceIndex - 4)

    def handleWhileLoop(self, statement):
        whileLoopConditionIndex = self.code.programIndex
        self.putExpressionIntoEax(statement.condition)
        self.code.testEaxEqualZero()
        self.code.conditionalJump('!=', 0xFFFFFFFF) ## jump if equal to zero (false) (skipping over while loop code)
        startWhileLoopCodeIndex = self.code.programIndex

        self.vars.enterScope()
        whilebodyLength = self.handleStatementList(statement.body.statements)
        self.vars.exitScope()

        ## specify how far to jump with a signed dword: (jumping back to while loop condition)
        self.code.unconditionalJump(whileLoopConditionIndex - self.code.programIndex)

        ## update the conditional jump for while loop condition with the length of the whileLoop code
        self.code.updateProgramAtIndex(toByteArray(whilebodyLength + 5), startWhileLoopCodeIndex - 4)


    def handleForLoop(self, statement):
        self.vars.enterScope()
        self.handleVariableDeclaration(statement.initalization)

        forLoopConditionIndex = self.code.programIndex
        self.putExpressionIntoEax(statement.condition)
        self.code.testEaxEqualZero()
        self.code.conditionalJump('!=', 0xFFFFFFFF) ## jump if equal to zero (false) (skipping over for loop code)
        startforLoopCodeIndex = self.code.programIndex

        self.handleStatementList(statement.body.statements)
        self.handleVariableAssignment(statement.increment)
        self.vars.exitScope()

        forbodyLength = self.code.programIndex - startforLoopCodeIndex

        ## specify how far to jump with a signed dword: (jumping back to for loop condition)
        self.code.unconditionalJump(forLoopConditionIndex - self.code.programIndex)

        ## update the conditional jump for for loop condition with the length of the forLoop code
        self.code.updateProgramAtIndex(toByteArray(forbodyLength + 5), startforLoopCodeIndex - 4)

    def handleFunctionCall(self, functionCall):
        ## Store result into EAX always?
        funcName = functionCall.functionName.token.value
        if funcName == 'putchar':
            self.sysFuctions.putchar(functionCall.arguments[0])
        elif funcName == 'getchar':
            self.sysFuctions.getchar()
        elif funcName == 'printf':
            self.sysFuctions.printf(functionCall.arguments)
        else:
            functionArgs = self.functions.getFunctionArgs(funcName)
            if len(functionArgs) != len(functionCall.arguments):
                throwError('Error Incorrect numbr of arguments: ' + funcName + ' ' + str(len(functionArgs)) + ' vs ' + str(len(functionCall.arguments)))
            for argument in reversed(functionCall.arguments):
                self.putExpressionIntoEax(argument)
                self.code.pushEAX() ## Push EAX to stack ## To Do functions can take varrying size arguments

            self.code.callRelative(0xFFFFFFFF)
            indexFromFuncStart = self.code.programIndex - self.functions.mostRecentFuncStartIndex
            self.functions.functionCalls.append({
                'name': functionCall.functionName.token.value,
                'indexFromFuncStart': indexFromFuncStart,
                'callingFunction': self.functions.mostRecentFuncStartName
             })
            for argument in functionCall.arguments: self.code.popECX()


    def handleFunctionDeclaration(self, statement):
        self.functions.mostRecentFuncStartIndex = self.code.programIndex
        self.functions.mostRecentFuncStartName = statement.functionName.token.value
        startIndex = self.code.programIndex

        newFunction = { 'name': statement.functionName.token.value, 'args': [] }
        self.functions.functions.append(newFunction)

        self.vars.enterScope()
        varCount = 0
        for argument in statement.arguments:
            varCount += 1
            newFunction['args'].append(argument.variableTypeNode.token.value)
            ####################mov eax, esp + (varCount * varDataSize)
            self.code.moveVarInStackToEAX(varCount * 4)
            self.vars.createBssVarFromEaxValue(argument.variableNameNode.token.value, 'int', self.code)

        self.handleStatementList(statement.functionBodyNode.statements)
        self.vars.exitScope()
        self.code.addReturn()
        newFunction['code'] = self.code.takeProgramBytesFrom(startIndex)  ## like no code was ever added to programBytes

    def handleReturnStatement(self, statement):
        self.putExpressionIntoEax(statement.returnExpression)
        self.code.addReturn()

    def handleMainFunction(self, statement):
        self.code.callRelative(9)  # call main
        self.code.movEAXtoEBX() ## Put exit code in EBX
        self.code.setEAXtoSysExit()
        self.code.callKernel()
        self.functions.functions.append({ 'name': 'main', 'startIndex': len(self.code.programBytes)})
        self.functions.mostRecentFuncStartIndex = self.code.programIndex
        self.functions.mostRecentFuncStartName = statement.functionName.token.value
        self.handleStatementList(statement.functionBodyNode.statements)


    def handleFullProgram(self, statements):
        ### ----- processing order
        ### handle function declarations and variable declarations in order
        ### putting functions into byte arrays
        ### putting saving functionCalls with placeholders
        ### variable declartions get added to program bytes

        processedMain = False
        for statement in statements:
            if processedMain: throwWarning('Warning: Statements after main function will not be reachable')
            if statement.type == 'function':
                if statement.functionName.token.value != 'main':
                    self.handleFunctionDeclaration(statement)
                else:
                    processedMain = True
                    self.functions.mostRecentFuncStartName = 'main'
                    self.handleMainFunction(statement)
            elif statement.type == 'declaration statement':
                self.handleVariableDeclaration(statement)
            else:
                throwError('Only declaration statements allowed outside of function body')

        ### loop through all function bytes
        ### add bytes to the end of programBytes after main code
        ### foreach loop through the functionCalls and put in correct location

        ### Linking Step
        self.functions.addExternalFunctions(self)

        for calledFunction in reversed(self.functions.functions):
            if calledFunction['name'] == 'main': continue
            calledFunction['startIndex'] = len(self.code.programBytes)

            self.code.addByteArrayToProgram(calledFunction['code'])
            for call in self.functions.functionCalls:
                if call['name'] == calledFunction['name']:
                    callingFunction = self.functions.getFunctionByName(call['callingFunction'])
                    if callingFunction == None: throwError('Error - function ' + str(call['callingFunction']) + ', not found')
                    location = call['indexFromFuncStart'] + callingFunction['startIndex']
                    #programBytes[Absolute Location To functionCall] = relative offset difference from functionCall to start of function
                    if calledFunction['startIndex'] - location < 0:
                        relativeAddrBytes = toByteArray(4294967296 + (calledFunction['startIndex'] - location))
                        self.code.updateProgramAtIndex(relativeAddrBytes, location - 4)
                    else:
                        relativeAddrBytes = toByteArray(calledFunction['startIndex'] - location)
                        self.code.updateProgramAtIndex(relativeAddrBytes, location - 4)

    def handleStatementList(self, statements):
        self.vars.enterScope()
        startIndex = self.code.programIndex
        for statement in statements:
            if statement.type == 'function':
                self.handleFunctionDeclaration(statement)
            elif statement.type == 'declaration statement':
                self.handleVariableDeclaration(statement)
            elif statement.type == 'assignment statement':
                self.handleVariableAssignment(statement)
            elif statement.type == 'return statement':
                self.handleReturnStatement(statement)
            elif statement.type == 'operator expression':
                self.handleOperationExpression(statement)
            elif statement.type == 'if statement':
                self.handleIfStatement(statement)
            elif statement.type == 'function call':
                self.handleFunctionCall(statement)
            elif statement.type == 'while loop':
                self.handleWhileLoop(statement)
            elif statement.type == 'for loop':
                self.handleForLoop(statement)
            else: throwError('Error - Unknown statement type')
        self.vars.exitScope()
        return self.code.programIndex - startIndex

    def generateMachineCode(self, tree):

        self.handleFullProgram(tree.statements)

        if len(self.code.programBytes) > 9576:
            throwError('Error program is too long. programIndex: ' + str(self.code.programIndex))

        return self.code.getFinalOutput()