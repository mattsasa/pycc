import sys
from colorama import Fore

class BasicObject(): pass

def split_at_symbol(sym, str):
    arr = str.split(sym)
    i = 1
    while i < len(arr):
        arr.insert(i, sym)
        i += 2
    arr = list(filter(lambda x: x != '', arr))
    return arr

def printfStringSplit(string):
    arr = [string]
    i = 0
    for char in ('%s','%d'):
        if char in arr[i]:
            splits = split_at_symbol(char, arr[i])
            arr.pop(i)
            i -= 1
            j = i
            for new in splits:
                j += 1
                arr.insert(j, new)
            i += 1
    return arr

def formatStrToCharArr(string):
    mainStr = string[1:-1]
    splits = printfStringSplit(mainStr)

    charListArr = []
    for split in splits:
        if split in ('%s','%d'):
            charListArr.append(split)
            continue
        charList = []
        i = 0
        while i < len(split):
            if ord(split[i]) == 92:
                charList.append(10)
                i += 1
            else: charList.append(ord(split[i]))
            i += 1
        charListArr.append(charList)
    return charListArr

def toByteArray(number):
    result = divmod(number, 16777216)
    byte1 = result[0]
    result = divmod(result[1], 65536)
    byte2 = result[0]
    result = divmod(result[1], 256)
    byte3 = result[0]
    byte4 = result[1]
    return [ byte4, byte3, byte2, byte1 ]

def throwError(str):
    print(Fore.RED + str + Fore.WHITE)
    sys.exit(1)

def throwWarning(str):
    print(Fore.YELLOW + str + Fore.WHITE)
